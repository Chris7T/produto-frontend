# Produto Frontend

## Resumo

Sistema para cadastro de produtos e categoriza-los

## Backend

Sistema Web feito em conjunto com a API `https://gitlab.com/Chris7T/produto-api`.

## Instacão Local

Instalação das Dependências 

```
npm install
```

## Servir

Servir com o angular

```
ng serve
```
