import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriaRoutingModule } from './categoria-routing.module';
import { FormularioComponent } from './formulario/formulario.component';
import { CategoriaListagemComponent } from './listagem/listagem.component';
import { CategoriaComponent } from './categoria.component';
import { GeraisModule } from '../Gerais/gerais.module';
import { CategoriaService } from 'src/app/Services/categoria.service';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbAlertModule, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    FormularioComponent,
    CategoriaListagemComponent,
    CategoriaComponent
  ],
  imports: [
    CommonModule,
    CategoriaRoutingModule,
    GeraisModule,
    ReactiveFormsModule,
    BrowserModule,
    NgbAlertModule
  ],
  exports:[
    FormularioComponent,
    CategoriaListagemComponent,
    CategoriaRoutingModule
  ],
  providers: [
    NgbModalConfig,
    NgbModal,
    CategoriaService
  ]
})
export class CategoriaModule { }
