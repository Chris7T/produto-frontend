import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CategoriaService } from 'src/app/Services/categoria.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'categoria-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  @ViewChild('templateVoltar') templateVoltar

  private id:number;
  private acao:string;
  public formulario:FormGroup
  public botao:string;
  public mostrarBotao:Boolean
  public formularioDesativado:Boolean
  public titulo:string
  public modalRef?: BsModalRef
  public mensagem: string

  constructor(
    private route : ActivatedRoute,
    private formBuilder : FormBuilder,
    public service : CategoriaService,
    private modalService: BsModalService

  ) {
    this.formulario = this.formBuilder.group({
      id: [null],
      nome: ['', [Validators.required, Validators.maxLength(100)]],
      taxa: ['', [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)]],
    });
  }

  ngOnInit(): void 
  {
    this.acao = this.route.snapshot.url[0].path;
    
    if(this.acao == 'buscar'){
      this.titulo = 'Visualização'
      this.mostrarBotao = false
      this.formularioDesativado = true
    }
    else {
      if(this.acao == 'editar'){
        this.titulo = 'Edição'
      }
      if(this.acao == 'adicionar'){
        this.titulo = 'Cadastrar'
      }
      this.mostrarBotao = true
      this.formularioDesativado = false
    }
    if(this.acao != 'adicionar'){
      this.buscar()
    }
  }
  
  getTitulo():string 
  {
    return this.titulo
  }
  buscar():void 
  {
    this.route.params.subscribe(
      (parametros: any) => {
        this.id = parametros['id']
      }
    )
    this.service.buscar(this.id).subscribe((dados:any) => {
      this.formulario.setValue(dados.data)
    })
  }
  salvar():void 
  {
    if(this.formulario.valid){
      if(this.acao == 'adicionar'){
        this.service.criar(this.formulario.value).subscribe(
          () => { this.mensagem = 'Categoria cadastrada.' },
          () => { this.mensagem = 'Erro ao cadastradar os dados, por favor tente mais tarde.'}
        )
      }
      if(this.acao == 'editar'){
        this.service.editar(this.id, this.formulario.value).subscribe(
          () => { this.mensagem = 'Categoria atualizada' },
          () => { this.mensagem = 'Erro ao atualizadar os dados, por favor tente mais tarde.'})
      }
      this.abrirConfirmacao()
    }
  }

  abrirConfirmacao(): void 
  {
    this.modalRef = this.modalService.show(this.templateVoltar);
  }
}
