
import { Component, OnInit } from '@angular/core';
import { CategoriaService } from 'src/app/Services/categoria.service';
import { FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'categoria-listagem',
  templateUrl: './listagem.component.html',
  styleUrls: ['./listagem.component.css']
})

export class CategoriaListagemComponent implements OnInit {

  public categorias: any[]
  public colunas:any
  public campos:any
  public rota:string
  public titulo:string
  public paginacao:FormGroup

  constructor(
    public service:CategoriaService
  ) {
    
    this.paginacao  = new FormGroup({
      pagina: new FormControl(1),
    });
    this.preencherTabela()
  }

  ngOnInit(): void 
  {
    this.colunas = [
      'Nome', 'Taxa'
    ]
    this.campos = [
      'nome', 'taxa'
    ]
    this
    this.titulo = 'Categoria'
    this.rota = 'categoria'
    this.paginacao.valueChanges.subscribe(valor => this.preencherTabela())
  }
 
  atualizarPagina(page):void
  {
    this.paginacao.setValue({ pagina:page })
  }
  
  preencherTabela(): void
  {
    this.service.listagem(this.paginacao.get('pagina').value)
    .subscribe((dados:any) => {     
      this.categorias = dados.data
    })
  }
}
