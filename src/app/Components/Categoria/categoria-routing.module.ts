import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router';

import { CategoriaListagemComponent } from './listagem/listagem.component';
import { FormularioComponent } from './formulario/formulario.component';

const categoriaRoutes: Routes = [
  { path: 'categoria', children:
    [
      { path: '', component:CategoriaListagemComponent },
      { path: 'adicionar', component:FormularioComponent},
      { path: 'buscar/:id', component:FormularioComponent },
      { path: 'editar/:id', component:FormularioComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(categoriaRoutes)],
  exports: [RouterModule]
})
export class CategoriaRoutingModule { }
