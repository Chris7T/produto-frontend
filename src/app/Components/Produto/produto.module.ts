import { ProdutoRoutingModule } from './produto-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormularioComponent } from './formulario/formulario.component';
import { ProdutoListagemComponent } from './listagem/listagem.component';
import { ProdutoComponent } from './produto.component';
import { ProdutoService } from 'src/app/Services/produto.service';
import { GeraisModule } from '../Gerais/gerais.module';
import { ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CategoriaService } from 'src/app/Services/categoria.service';
import { NgbAlertModule, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    FormularioComponent,
    ProdutoListagemComponent,
    ProdutoComponent
  ],
  imports: [
    CommonModule,
    ProdutoRoutingModule,
    GeraisModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    NgbAlertModule
  ],
  exports: [
    FormularioComponent,
    ProdutoListagemComponent,
    ProdutoRoutingModule
  ],
  providers: [
    NgbModalConfig,
    NgbModal,
    ProdutoService,
    CategoriaService
  ]
})
export class ProdutoModule { }
