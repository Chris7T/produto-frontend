import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProdutoListagemComponent } from './listagem/listagem.component';
import { FormularioComponent } from './formulario/formulario.component';

const produtoRoutes: Routes = [
  { path: 'produto', children:
    [
      { path: '', component:ProdutoListagemComponent },
      { path: 'adicionar', component:FormularioComponent},
      { path: 'buscar/:id', component:FormularioComponent },
      { path: 'editar/:id', component:FormularioComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(produtoRoutes)],
  exports: [RouterModule]
})
export class ProdutoRoutingModule { }
