import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProdutoService } from 'src/app/Services/produto.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'produto-listagem',
  templateUrl: './listagem.component.html',
  styleUrls: ['./listagem.component.css']
})
export class ProdutoListagemComponent implements OnInit {

  public colunas:any
  public campos:any
  public rota:string
  public titulo:string
  public paginacao:FormGroup
  public produtos: any[]

  constructor(
    public service:ProdutoService
  ) {
    this.paginacao  = new FormGroup({
      pagina: new FormControl(1),
    });
    this.preencherTabela()
  }

  ngOnInit(): void 
  {
    this.colunas = [
      'Nome', 'Valor'
    ]
    this.campos = [
      'nome', 'valor'
    ]
    this
    this.titulo = 'Produto'
    this.rota = 'produto'
    this.paginacao.valueChanges.subscribe(valor => this.preencherTabela())
  }
  atualizarPagina(page): void 
  {
    this.paginacao.setValue({ pagina:page })
  }

  preencherTabela(): void 
  {
    this.service.listagem(this.paginacao.get('pagina').value)
    .subscribe((dados:any) => {
      this.produtos = dados.data
    })
  }
}
