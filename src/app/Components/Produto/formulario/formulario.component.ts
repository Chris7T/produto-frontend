import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProdutoService } from 'src/app/Services/produto.service';
import { ActivatedRoute } from '@angular/router';
import { CategoriaService } from 'src/app/Services/categoria.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';

@Component({
  selector: 'produto-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  @ViewChild('templateVoltar') templateVoltar
  @ViewChild('templateMensalidade') templateMensalidade

  private id:number;
  private acao:string;
  public formulario:FormGroup
  public formMensalidade:FormGroup
  public botao:string;
  public mostrarBotaoSalvar:Boolean
  public formularioDesativado:Boolean
  public categoria:any
  public categoriaPagina:number
  public mostrarDropDown:Boolean
  public categoriaInvalid:Boolean
  public titulo:string
  public modalRef?: BsModalRef
  public mensagem: string
  public mensalidades:Array<any>
  public mostrarMensalidade:Boolean
  public listaMensalidades: Observable<any>
  public semTipoJuros:Boolean

  constructor(
    private route : ActivatedRoute,
    private formBuilder : FormBuilder,
    public service : ProdutoService,
    public serviceAux : CategoriaService,
    private modalService: BsModalService
  ) {    this.formulario = this.formBuilder.group({
      id: [null],
      nome: ['', [Validators.required, Validators.maxLength(100)]],
      descricao: ['', [Validators.required, Validators.maxLength(100)]],
      valor: ['', [Validators.required, Validators.pattern(/^\d+(\.\d{1,2})?$/)]],
      categoria_id: ['', [Validators.required]],
      categoria_nome: [''],
    });
    this.formMensalidade = this.formBuilder.group({
      numero_parcelas: [1, [Validators.max(12), Validators.min(1), Validators.pattern("^[0-9]*$")]],
      produto_id: ['',  [Validators.required, Validators.pattern("^[0-9]*$")]],
      tipo_juros: ['', [Validators.required]],
    });

    this.categoria = []
    this.semTipoJuros = false
  }

  ngOnInit(): void {
    this.mostrarDropDown = false
    this.categoriaInvalid = false
    this.acao = this.route.snapshot.url[0].path;
    
    if(this.acao == 'buscar'){
      this.titulo = 'Visualização'
      this.mostrarBotaoSalvar = false
      this.formularioDesativado = true
    }
    else {
      if(this.acao == 'editar'){
        this.titulo = 'Edição'
      }
      if(this.acao == 'adicionar'){
        this.titulo = 'Cadastrar'
      }
      this.mostrarBotaoSalvar = true
      this.formularioDesativado = false
    }
    if(this.acao != 'adicionar'){
      this.buscar()
    }
    this.categoriaPagina = 0
    this.buscarCategorias()
  }
  
  getTitulo():string 
  {
    return this.titulo
  }

  buscar():void
  {
    this.route.params.subscribe(
      (parametros: any) => {
        this.id = parametros['id']
      }
    )
    this.service.buscar(this.id).subscribe((dados:any) => {
      this.formulario.setValue(dados.data)
    })
  }

  salvar():void
  {
    if(this.formulario.valid){
      if(this.acao == 'adicionar'){
        this.service.criar(this.formulario.value).subscribe(
          () => { this.mensagem = 'Categoria cadastrada.' },
          () => { this.mensagem = 'Erro ao cadastradar os dados, por favor tente mais tarde.'}
        )
      }
      if(this.acao == 'editar'){
        this.service.editar(this.id, this.formulario.value).subscribe(
          () => { this.mensagem = 'Categoria atualizada' },
          () => { this.mensagem = 'Erro ao atualizadar os dados, por favor tente mais tarde.'})
      }
      this.abrirConfirmacao()
    }
  }

  onScroll():void
  {
    this.buscarCategorias()
  }

  buscarCategorias():void
  {
      this.serviceAux.listagem(this.categoriaPagina).subscribe((dados:any) =>{
        dados.data.map((item)=>{
          this.categoria.push(item)
        })
        this.categoriaPagina++
      })
  }

  setCategoria(item):void
  {
    this.categoriaInvalid = false
    this.formulario.controls['categoria_id'].markAsDirty()
    this.formulario.controls['categoria_id'].markAsTouched()
    this.formulario.controls['categoria_id'].setValue(item.id)
    this.formulario.controls['categoria_nome'].setValue(item.nome)
  }

  esperaCategoria():void
  {
    this.categoriaInvalid = true
  }

  abrirConfirmacao():void 
  {
    this.modalRef = this.modalService.show(this.templateVoltar);
  }

  popUpMensalidade():void 
  {
    this.modalRef = this.modalService.show(this.templateMensalidade);
  }
  fecharPopUp():void
  {
    this.modalRef?.hide()
  }
  buscarMensalidades():void
  {
    if(this.formMensalidade.get('tipo_juros').errors?.required){
      this.semTipoJuros = true
    }
    else {
      this.semTipoJuros = false
    }
    this.formMensalidade.controls['produto_id'].setValue(this.id)
    if(this.formMensalidade.valid){
      this.listaMensalidades = this.service.buscarMensalidades(this.formMensalidade.value)
    }
  }
}
