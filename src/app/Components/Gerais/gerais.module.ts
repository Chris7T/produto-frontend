import { ProdutoService } from './../../Services/produto.service';
import { CategoriaService } from './../../Services/categoria.service';
import { NgbPaginationModule, NgbAlertModule, NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ListagemComponent } from './listagem/listagem.component';

@NgModule({
  declarations: [
    NotFoundComponent,
    HomeComponent,
    ListagemComponent
  ],
  imports: [
    CommonModule,
    NgbPaginationModule,
    NgbAlertModule
  ],
  exports: [
    NotFoundComponent,
    HomeComponent,
    ListagemComponent
  ],
  providers: [
    NgbModalConfig,
    NgbModal,
    CategoriaService,
    ProdutoService
  ]
})
export class GeraisModule { }
