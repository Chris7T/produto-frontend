import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Component, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listagem',
  templateUrl: './listagem.component.html',
  styleUrls: ['./listagem.component.css']
})
export class ListagemComponent implements OnInit {

  @Input() array:any
  @Input() arrayColunas:any
  @Input() arrayCampos:any
  @Input() rota:string
  @Input() titulo:string
  @Input() service:any
  @Input() paginacao:FormGroup
  @ViewChild('template') template
  
  public arrayLinhas:any
  public page:number
  public colecao:number 
  public modalRef?: BsModalRef
  public mensagem:string
  public detalhes:string

  constructor(
    private router: Router,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.page = 1
    this.arrayLinhas = []
  }

  ngOnChanges(): void 
  {
    if(this.arrayLinhas){
      this.prepararTabela();
    }
  }

  prepararTabela(): void 
  {
    this.arrayLinhas = []
    this.service.listagem(1).subscribe(dados =>{
      this.colecao = dados.meta.total
      this.array = dados.data
    })
    this.array.map((item) => {
      this.listarAtributos(item)
    })
  }

  listarAtributos(item): void 
  {
    var colunas = []
    Object.keys(item).map((atributo) =>{
      if(this.arrayCampos.includes(atributo) || atributo == 'id'){
        colunas.push(item[atributo])
      }
    })
    this.arrayLinhas.push(colunas)
  }

  getTitulo(): string
  {
    return this.titulo
  }

  adicionarRedirect(): void 
  {
    this.router.navigate(['/' + this.rota +'/adicionar']);
  }

  buscarRedirect(item): void 
  {
    this.router.navigate(['/' + this.rota +'/buscar/' + item[0]]);
  }

  editarRedirect(item): void 
  {
    this.router.navigate(['/' + this.rota +'/editar/' + item[0]]);
  }

  apagarRedirect(item): void 
  {
    this.openModal()
    this.service.apagar(item[0]).subscribe((dados:any) => {
      this.mensagem = this.titulo + ' apagado com sucesso.'
      this.atualizarPagina(this.page)
    },
    (erro:any)=>{
     if(erro.status){
       this.mensagem = 'Não foi possivel apagar a ' + this.titulo + '.'
     }
    })
  }
  atualizarPagina(page): void 
  {
    this.paginacao.setValue({ pagina:page })
  }
  openModal(): void 
  {
    this.modalRef = this.modalService.show(this.template);
  }
}
