import { ProdutoModule } from './Components/Produto/produto.module';
import { CategoriaModule } from './Components/Categoria/categoria.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './Components/Gerais/header/header.component';
import { GeraisModule } from './Components/Gerais/gerais.module';
import { BsModalService } from 'ngx-bootstrap/modal';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    CategoriaModule,
    ProdutoModule,
    AppRoutingModule,
    GeraisModule,
    ReactiveFormsModule,
    NgbModule,
    TooltipModule.forRoot(),
    BrowserAnimationsModule  
  ],
  bootstrap: [
    AppComponent
  ],
  providers: [
    BsModalService
  ],
})
export class AppModule { }
