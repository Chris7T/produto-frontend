import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ProdutoModel } from '../Model/ProdutoModel';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  private url:string
  
  constructor(private http: HttpClient) 
  {
    this.url = environment.API_URL + '/produto'
  }

  listagem(pagina:number)
  {
    return this.http.get(pagina!=1 
      ? this.url + "?page=" + pagina 
      : this.url 
    )
  }

  criar(produto:ProdutoModel)
  {
    return this.http.post(this.url, produto)
  }

  buscar(id:number)
  {
    return this.http.get(this.url + '/' + id)
  }

  editar(id:number, produto:ProdutoModel)
  {
    return this.http.put(this.url + '/' + id, produto)
  }

  apagar(id:number)
  {
    return this.http.delete(this.url + '/' + id)
  }

  buscarMensalidades(mensalidadeDados)
  {
    return this.http.post(this.url + '/mensalidade', mensalidadeDados)
  }
}
