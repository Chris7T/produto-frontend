import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from 'src/environments/environment';
import { CategoriaModel } from '../Model/CategoriaModel';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  private url:string

  constructor(private http: HttpClient) 
  {
    this.url = environment.API_URL + '/categoria'
  }

  listagem(pagina:number)
  {
    return this.http.get(pagina!=1 
      ? this.url + "?page=" + pagina 
      : this.url 
    )
  }

  criar(categoria:CategoriaModel)
  {
    return this.http.post(this.url, categoria)
  }

  buscar(id:number)
  {
    return this.http.get(this.url + '/' + id)
  }

  editar(id:number, categoria:CategoriaModel)
  {
    return this.http.put(this.url + '/' + id, categoria)
  }

  apagar(id:number)
  {
    return this.http.delete(this.url + '/' + id)
  }
}
